<?php

namespace Drupal\Tests\custom_elements\Kernel;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\KernelTests\KernelTestBase;
use Drupal\custom_elements\CustomElementGeneratorTrait;
use Drupal\custom_elements\CustomElementsFieldFormatterInterface;
use Drupal\custom_elements\Entity\EntityCeDisplay;

/**
 * Tests the entity custom element display configuration entities.
 *
 * @group custom_elements
 */
class EntityCeDisplayTest extends KernelTestBase {

  use CustomElementGeneratorTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   *
   * @var bool
   * @todo Fix config schema for CE-displays and re-enable.
   */
  protected $strictConfigSchema = FALSE;

  /**
   * Modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'custom_elements',
    'entity_test',
    'user',
    'field_test',
  ];

  /**
   * Tests basic CRUD operations on entity custom element display objects.
   */
  public function testEntityCeDisplayCrud() {
    $ce_display = EntityCeDisplay::create([
      'targetEntityType' => 'entity_test',
      'customElementName' => 'name_test',
      'bundle' => 'entity_test',
      'mode' => 'default',
    ]);

    // Check that arbitrary options are correctly stored.
    $expected['component_1'] = [
      'weight' => 10,
      'field_name' => 'component',
      'configuration' => [
        'third_party_settings' => ['field_test' => ['foo' => 'bar']],
        'settings' => [],
      ],
    ];
    $ce_display->setComponent('component_1', $expected['component_1']);
    $this->assertEquals($expected['component_1'], $ce_display->getComponent('component_1'));

    // Check that the display can be properly saved and read back.
    $ce_display->save();

    // Load the entity.
    $ce_display = EntityCeDisplay::load($ce_display->id());
    $this->assertNotNull($ce_display, 'The entity was loaded successfully.');
    $this->assertEquals($expected['component_1'], $ce_display->getComponent('component_1'));

    // Testing saving auto-processing.
    $ce_display->setForceAutoProcessing(TRUE);
    $ce_display->save();
    $ce_display = EntityCeDisplay::load($ce_display->id());
    $this->assertTrue($ce_display->getForceAutoProcessing());

    // Delete the entity.
    $ce_display_id = $ce_display->id();
    $ce_display->delete();
    $this->assertNull(EntityCeDisplay::load($ce_display_id), 'The entity was deleted successfully.');
  }

  /**
   * Tests config of core field formatters is correctly used.
   */
  public function testCoreFieldFormatterComponents() {
    $ce_display = EntityCeDisplay::create([
      'targetEntityType' => 'entity_test',
      'customElementName' => 'name_test',
      'bundle' => 'entity_test',
      'mode' => 'default',
    ]);
    $ce_display->setComponent('user', [
      'field_name' => 'user_id',
      'is_slot' => TRUE,
      'formatter' => 'field:entity_reference_entity_view',
      'configuration' => [
        'settings' => ['view_mode' => 'some_teaser'],
      ],
    ]);
    // Save and load to make sure config stays.
    $ce_display->save();
    $ce_display = EntityCeDisplay::load($ce_display->id());
    $formatter = $ce_display->getRenderer('user');
    $this->assertEquals('user', $formatter->getConfiguration()['name']);
    $this->assertEquals(TRUE, $formatter->getConfiguration()['is_slot']);
    $this->assertInstanceOf(CustomElementsFieldFormatterInterface::class, $formatter);
    $this->assertEquals(
      [$this->t('Rendered as @mode', ['@mode' => 'some_teaser'])],
      $formatter->settingsSummary()
    );
    $this->assertEquals('some_teaser', $formatter->getSetting('view_mode'));
  }

}
