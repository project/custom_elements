# Custom elements

## Introduction

The Custom Elements module provides the framework for rendering Drupal data
(entities, fields, ...) into custom elements markup. Custom elements can be
easily rendered by frontend components, e.g. via web components or various
Javascript frontend frameworks. This enables Drupal to render into high-level
theme components, while the actually rendering of the components is handled by
a frontend application (possibly in the browser).

The Custom Elements module provides
 * the UI to configure the custom element output for entities by view-mode (3.x only)
 * the API to build a (nested tree) of custom element objects, with associated
cache metadata
 * the API to serialize a tree of custom objects into markup or into
a JSON representation
 * the API for other modules to customize how data is rendered into custom
elements via Custom element processors

### Frontend rendering

Today's browsers provide [an API](https://html.spec.whatwg.org/multipage/custom-elements.html#custom-elements-autonomous-example)
for developers to define their own HTML elements, like
`<flag-icon country="nl"></flag-icon>`. Besides that, many frontend frameworks
render their components using the same, or similar custom elements syntax.
That way, we can render a custom element with [Web components](https://developer.mozilla.org/de/docs/Web/Web_Components) or any component-oriented frontend
framework, like [Vue.js](https://vuejs.org/) or [React](https://react.dev/).

## Usage

### Progressive decoupling

Custom elements output may be used as part of a regular Drupal-rendered
response, where some JavaScript or Web-Components take over the rendering in
the browser. For that use case, any Javascript libraries needed for rendering
the markup may be added to the `custom_elements/main` library, e.g. in a custom
theme. This library is attached to every custom element markup rendered via
Drupal.

To use custom element markup for outputting specific entity bundles / view modes,
enable the "Force custom elements rendering" checkbox on their "Manage Display" tab.

### Full decoupling

Alternatively, the module can be used in conjunction via the Lupus Custom Elements Renderer
or Lupus Decoupled Drupal modules. The [Lupus Custom Elements Renderer](https://www.drupal.org/project/lupus_ce_renderer)
module switches Drupal's main content renderer to provide API responses using
custom elements markup or a custom elements JSON serialization for complete
pages.

### Lupus Decoupled Drupal

Lupus Decoupled Drupal builds upon Lupus Custom Elements Renderer and packages
it up into an easily usable decoupled setup, see https://lupus-decoupled.org

## Custom element render formats

The module supports two main serialization modes:

* Markup - Render custom elements into HTML-like markup.
* JSON - Render a JSON tree representation of custom elements.

The markup variant supports multiple styles to cater for the different needs
of different frameworks.

### Custom Element markup styles

Custom elements use "slots" for handling content distribution, i.e. for passing
nested content to an element. However, the concrete syntax used for handling
slots may differ by various frameworks. Thus, the module supports rendering to
different markup styles while it defaults to the Web component style syntax,
which is supported by Vue 2 as well (via its legacy slot syntax). In addition,
the module supports the [Vue2 and Vue 3](https://vuejs.org/v2/guide/components-slots.html#Named-Slots-Shorthand)
syntax which can be enabled via config:

    drush config:set custom_elements.settings markup_style vue-3


## Further details

### Methods of rendering custom elements

By default, the module does nothing unless its API is used by other modules
(e.g. the Lupus CE renderer) or it's configured to take over entity rendering
for some entity view modes (per "Progressive decoupling" above).

An entity-view can be rendered into custom elements by one of three methods.
Methods 1 and 2 need a Custom Elements Display (configuration object); this can
be viewed/edited through the Drupal UI after enabling the Custom Elements UI
submodule.

1. Using layouts

This method renders a layout, as configured with Drupal's layout builder, into
`<drupal-layout>` elements and allows the contained blocks to render into
custom elements.

This method is used when two conditions are both met:
* "Use Layout Builder" is enabled for the requested view mode's display (or for
  the default display if none exists for the requested view mode);
* "Use Layout Builder" is enabled for the requested view mode's Custom Elements
  Display (or for the default display if none exists for the requested view
  mode). This option is ignored (and invisible in the UI) if the same option
  isn't enabled in the regular display.

2. Using automatic processing with processors

The module comes with Custom element processors that try to implement
a reasonable default for content entities and fields. These defaults can be
further customized with custom modules.

The default entity processor renders the data of all visible fields either as
attribute to the custom element tag, or as nested markup via a slot. The module
maps simple fields and their properties to attributes and falls back to
rendering more complex fields to regular markup, which gets added as slot to
the parent custom element.

This was the default in version 2.x and is still used when "Use automatic
processing" is enabled in the Custom Elements display.

Processor services can also be used to render individual fields; this is still
the default (i.e. a newly created Custom Elements Display has all fields set
to the "Automatic processing" formatter.)

3. Using components in a Custom Elements Display

A Custom Elements Display contains information to render individual fields
using 'custom element field formatter plugins'; values are formatted and output
as configured by each plugin. This is equivalent to field formatters used by
Drupal's regular displays.

This is the default method in version 3.x. If no Custom Elements Displays are
created yet for a certain entity/bundle, a display is created internally for
rendering, based on fields enabled in the regular display (for the specific
view mode if it's enabled; otherwise for the default view mode). All fields are
rendered in "Automatic processing" mode (using the "Auto" formatter).


## Upgrade from 2.x

Generally, the core module and its API remain unchanged. However, with the
addition of configuration based rendering, the default output of an entity
rendered as custom element changed. Besides that some small API changes /
simplifications have been applied.

Please take a list of change records for details:

https://www.drupal.org/list-changes/custom_elements


### Submodules

* custom_elements_everywhere:
  The module is gone in 3.x - please uninstall it before updating.
  Instead, simply enable "force custom elements rendering" for all view
  modes as desired. This can be done in the corresponding "Manage display"
  tab in the UI.
  In terms of configuration: in each applicable
  core.entity_view_display.TYPE.BUNDLE.VIEWMODE configuration entity, set
  third_party_settings.custom_elements.enabled = true.

* custom_elements_thunder:
  Version 2.x implements a couple of example processors for the thunder
  paragraphs. Those have been removed and replaced by Custom Element
  display configuration that works similarly.
  See [the module's README.md](modules/custom_elements_thunder/README.md)
  for further details.

### Updating from 3.0.0-alpha1

Version 3.0.0-alpha1 had a different structure for 'entity_ce_display' config
entities. For converting existing entities saved in this version (in either
configuration files or active storage), update to 3.0.0-alpha2 first, and see
https://www.drupal.org/node/3455435.

### Plan your custom update path

The update introduces breaking changes in the way entities are rendered, and
there is no automatic update path. (The only thing that is done automatically,
is enabling the Custom Elements UI submodule.) Reasons:

* Creating 'version 2 compatible' Custom Elements Displays (i.e. with
  automatic processing enabled) for every entity type + bundle may be
  detrimental in the longer term, because
  * Your Drupal installation will contain a lot of unused configuration.
  * Especially when entities are built using the default entity processor in
    version 2, this is likely to produce the same output with new version 3
    defaults for a Custom Elements Display. These defaults do not need a
    display to be pre-created, and also provide more flexibility for future
    configuration through the UI than having "automatic processing" enabled.
* Custom Elements Displays with automatic processing enabled still don't provide
  perfect backward compatibility, e.g. [the "type" attribute is removed](https://www.drupal.org/node/3474724).
* Any view mode that uses Layout Builder to build custom elements, now
  requires a Custom Elements Display that enables this option. However, it is
  not easy to automatically detect whether Layout Builder is used: in version 2,
  this not only depends on configuration, but also on whether entities are built
  by a custom processor (in another module). See below code comments for more
  detailed info.

Example code is provided below, that could create Custom Elements Displays with
automatic processing enabled. Please weigh the following options, from 'worst'
to 'best' in terms of future flexibility:
* During the upgrade, create Custom Elements Displays that enable automatic
  processing everywhere, per below code.
* Make a list of which exact bundles + view modes you use for rendering custom
  elements, and only create Custom Elements Displays with automatic
  processing for those.
* Make the same list, and test if your output is the same when using Custom
  Elements Displays with default settings (which do not need to be precreated
  and saved in active configuration), and/or if you can tweak Custom Elements
  Displays slightly to get a desired result.

Regardless of your choice: if you are using Layout Builder then some Custom
Elements Displays will need to be precreated, and some things will need to be
checked. Below code does this / outputs notices.

```php
  // Version 3 must be installed already.
  //
  // FALSE creates only CE displays with "Layout Builder" enabled (which are
  // required), and skips creating CE displays with just "automatic processing".
  $create_auto_ce_displays = FALSE;

  /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $core_display_repo */
  $core_display_repo = \Drupal::service('entity_display.repository');
  $ce_storage = \Drupal::entityTypeManager()->getStorage('entity_ce_display');
  $logger = class_exists('Drush\Drush') ? \Drush\Drush::logger() : \Drupal::logger('custom_elements');

  foreach ($ALL_APPROPRIATE_ENTITY_TYPES as $entity_type_id) {
    $view_modes = $core_display_repo->getViewModes($entity_type_id);
    $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entity_type_id);
    foreach (array_keys($bundles) as $bundle_key) {
      $core_display = $core_display_repo->getViewDisplay($entity_type_id, $bundle_key);
      $layout_builder_default = $core_display->getThirdPartySetting('layout_builder', 'enabled') && $core_display->status();
      // Skip already existing CE displays.
      if (!$ce_storage->load("$entity_type_id.$bundle_key.default")) {
        // Create CE display; remove default components/fields, to signify
        // these are not used. (Unfortunately, at least in v3.0.1 the UI still
        // initializes and displays new components when it finds an empty list.)
        $ce_display = EntityCeDisplay::create(['mode' => 'default', 'targetEntityType' => $entity_type_id, 'bundle' => $bundle_key]);
        foreach (array_keys($ce_display->getComponents()) as $name) {
          $ce_display->removeComponent($name);
        }
        // If Layout Builder is enabled on default Core display, then enable it
        // here too; otherwise enable automatic processing.
        if ($layout_builder_default) {
          $ce_display->setUseLayoutBuilder(TRUE);
          // In v2, a non-default view mode would use a Layout,
          // * IF it does not have its own (Core) Entity View Display, or if
          //   its own Entity View Display has Layout Builder enabled,
          // * AND IF it is not handled by a custom processor that does not
          //   extend DefaultContentEntityProcessor.
          // It is hard to decide whether it makes more sense
          // 1. whether to enable Layout Builder on the default CE display, and
          //    then also create CE displays with layout builder explicitly
          //    disabled for any view mode that needs it,
          // 2. or to keep Layout Builder disabled on the default CE display,
          //    and CE displays with layout builder enabled for any view mode
          //    that needs it,
          // ...because it is unknown whether entities are ever built using
          // the "default" view mode explicitly. (If so, then only the first
          // option is possible.) Leave it up to the user.
          $logger->notice("$entity_type_id.$bundle_key has Layout Builder enabled for the 'default' view mode. Please reconsider if you:
1) want building custom elements in any view mode to default to using Layout Builder; in this case, a CE display must be added for any view mode which has its own Core Display (that doesn't use Layout Builder), OR which uses a custom processor (that does not extend DefaultContentEntityProcessor);
2) prefer building custom elements to not default to using Layout Builder; in this case, the \"Use Layout Builder\" option must be disabled on the default CE display and a CE display must be added for any view mode which should use Layout Builder.");
        }
        $ce_display->setForceAutoProcessing(TRUE);
        if ($layout_builder_default || $create_auto_ce_displays) {
          $ce_display->save();
        }
      }
      if (!$layout_builder_default) {
        // Loop through non-default view modes; see if Layout Builder is enabled.
        // Note: if not enabled, the display doesn't necessarily exist in config.
        foreach (array_keys($view_modes) as $view_mode_key) {
          $core_display = $core_display_repo->getViewDisplay($entity_type_id, $bundle_key, $view_mode_key);
          $layout_builder = $core_display->getThirdPartySetting('layout_builder', 'enabled') && $core_display->status();
          if ($layout_builder && !$ce_storage->load("$entity_type_id.$bundle_key.$view_mode_key")) {
            // Inverse of above: if the view mode is handled by a custom
            // processor, then Layout Builder was likely not used in version 2,
            // and we are likely fine. If it is handled by
            // DefaultContentEntityProcessor or a custom processor that extends
            // it, we need a CE display with Layout Builder explicitly enabled.
            $logger->notice("$entity_type_id.$bundle_key has Layout Builder enabled for the '$view_mode_key' view mode. If NO custom processor is used, a CE display must still be added, with 'Layout builder' enabled. If a custom processor is using a Layout for building: check https://www.drupal.org/node/3485395.");
          }
        }
      }
    }
  }
```

## Credits

  - [drunomics GmbH](https://www.drupal.org/drunomics): Concept, Development, Maintenance
  - [Österreichischer Wirtschaftsverlag GmbH](https://www.drupal.org/%C3%B6sterreichischer-wirtschaftsverlag-gmbh): Initial sponsor of v1
