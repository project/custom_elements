<?php

namespace Drupal\custom_elements\Plugin\CustomElementsFieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\custom_elements\CustomElement;
use Drupal\custom_elements\CustomElementGenerator;
use Drupal\custom_elements\CustomElementsFieldFormatterBase;

/**
 * Implementation of the 'auto' custom element formatter plugin.
 *
 * @CustomElementsFieldFormatter(
 *   id = "auto",
 *   label = @Translation("Auto"),
 *   weight = -10
 * )
 */
class AutoCeFieldFormatter extends CustomElementsFieldFormatterBase {

  /**
   * Custom elements generator.
   *
   * @var \Drupal\custom_elements\CustomElementGenerator
   */
  protected CustomElementGenerator $ceGenerator;

  /**
   * Construct.
   *
   * @param object $configuration
   *   Configuration.
   * @param string $plugin_id
   *   Plugin ID.
   * @param object $plugin_definition
   *   Plugin definition.
   * @param \Drupal\custom_elements\CustomElementGenerator $ce_generator
   *   Custom element generator.
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, CustomElementGenerator $ce_generator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->ceGenerator = $ce_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create($container, array $configuration, $plugin_id, $plugin_definition) {
    // @phpstan-ignore-next-line
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('custom_elements.generator'));
  }

  /**
   * {@inheritdoc}
   */
  public function build(FieldItemListInterface $items, CustomElement $custom_element, $langcode = NULL) {
    // Have a processor do its work; ignore our is_slot configuration value.
    $this->ceGenerator->process($items, $custom_element, $this->getViewMode(), $this->getName());
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // Nothing needed.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Nothing needed.
  }

}
