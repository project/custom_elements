<?php

namespace Drupal\custom_elements;

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\Element;

/**
 * Helper trait for CE elements content rendering.
 */
trait CustomElementsBlockRenderHelperTrait {

  /**
   * Converts the block content render array into custom elements.
   *
   * @param array $build
   *   The render array.
   * @param \Drupal\custom_elements\CustomElement $parent_element
   *   The parent custom element to which content will be added.
   *
   * @return \Drupal\custom_elements\CustomElement[]
   *   The list of generated custom elements.
   */
  public function getElementsFromBlockContentRenderArray(array $build, CustomElement $parent_element) {
    $elements = [];

    foreach (Element::children($build, TRUE) as $key) {
      if (isset($build[$key]['#cache'])) {
        $parent_element->addCacheableDependency(BubbleableMetadata::createFromRenderArray($build[$key]));
      }
      // Handle empty cache-only entries.
      if (isset($build[$key]['#cache']) && count($build[$key]) == 1) {
        continue;
      }

      if (isset($build[$key]['content']['#custom_element'])) {
        $block_element = $build[$key]['content']['#custom_element'];
      }
      else {
        // Un-clear render item, just forward through in a drupal-block tag.
        $block_element = CustomElement::create('drupal-markup');
        $block_element->setSlotFromRenderArray('default', $build[$key]);
      }
      $elements[] = $block_element;
    }
    return $elements;
  }

}
