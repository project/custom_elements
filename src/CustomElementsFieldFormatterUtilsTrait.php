<?php

namespace Drupal\custom_elements;

use Drupal\Core\Field\FieldItemInterface;

/**
 * Trait for custom elements field formatters.
 */
trait CustomElementsFieldFormatterUtilsTrait {

  /**
   * Returns property name/value pairs for a single field item, as an array.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $field_item
   *   The field to process.
   * @param string[] $ignore_properties
   *   (Optional) properties to ignore.
   *
   * @return array
   *   The field's property values keyed by property name. Always returned as
   *   an array, for easier extensibility by child classes.
   */
  protected function getFieldItemProperties(FieldItemInterface $field_item, array $ignore_properties = []): array {
    // Collect all values into an array. Note iterating through getValue() can
    // (depending on the field) produce different results than just
    // $values = $field_item->getValue().
    $values = [];
    foreach ($field_item->getValue() as $name => $value) {
      if (!in_array($name, $ignore_properties, TRUE)) {
        $values[$name] = $value;
      }
    }
    return $values;
  }

}
